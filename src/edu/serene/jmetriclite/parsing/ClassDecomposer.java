package edu.serene.jmetriclite.parsing;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.visitor.*;
import com.github.javaparser.ast.body.*;

public class ClassDecomposer extends VoidVisitorAdapter<Object> {

	private boolean aggregate = false;
	public ClassDecomposer(boolean doAggregate) {
		aggregate = doAggregate;
	}
	public void listClasses(File javaFile) throws ParseException, IOException {
		FileInputStream in = new FileInputStream(javaFile);
		
		CompilationUnit cu;
	    try {
	        // parse the file
	        cu = JavaParser.parse(in);
	    } finally {
	        in.close();
	    }

	    visit(cu, null);
	}
	public void visit(CompilationUnit n, Object arg) {
        super.visit(n, arg);
        //System.out.println(n.getName());
        List<TypeDeclaration> types = n.getTypes();
        String packageName = n.getPackage().toString().replaceAll("package", "").replace(";", "").replace("\n", "");
        for(TypeDeclaration type : types) {
        	
        	if(type instanceof ClassOrInterfaceDeclaration) {		
        			walkClass(packageName, (ClassOrInterfaceDeclaration)type);
        			//TODO public/private fields
        	}

        }
   }
	
	//TODO: abstract this into a ClassWalker class so that we can compute class-level metrics.
	public void walkClass(String packageName, ClassOrInterfaceDeclaration classDef) {
		
		System.out.println(packageName + "." + classDef.getName());
	
		List<BodyDeclaration> members = classDef.getMembers();
		boolean printHeader = true;
		for (BodyDeclaration member : members) {
		
			if (member instanceof MethodDeclaration) {
				//This is a method in the class, need to process the method statements...
				MethodWalker mw = new MethodWalker(classDef.getName(), aggregate, printHeader);
        		mw.walkMethod((MethodDeclaration)member, true);
        		printHeader = false; //only want one header per class stats dump.
        	}
			
			if(member instanceof ClassOrInterfaceDeclaration) {
				//This is a nested class, need to walk its entire tree as well...
				walkClass(packageName, (ClassOrInterfaceDeclaration)member);
			}
		}
	}
	
}
