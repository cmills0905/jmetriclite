package edu.serene.jmetriclite.parsing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.Range;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.Comment;

public class MethodWalker {
	protected int linesOfComments = 0;
	
	private String name = "";
	private String className = "";
	//LOC
	protected int loc = 0;
	private int rloc = 0;
	
	//this is used in the McCabe's CC computation. 
	private int numberOfSwitchCases = 0;	//For each switch-case, we want to consider cases beyond 1 to be additional complexity. (i.e. a 4 part switch is more complex than a 1 part switch)
	private int numberOfReturns = 0; 		//We should include all return statements beyond 1 (i.e. only count the ones that aren't the last one)
	protected int mccabesComplexity = 1;
	
	//this is all Halstead stuff
	//TODO: You know, maybe we don't need to store the full symbol table for the method... need to re-evaluate at some point
	protected HashSet<String> uniqueOperators = new HashSet<String>(); //for computing the unique operators
	protected HashSet<String> uniqueOperands = new HashSet<String>(); //for computing the unique operands
	protected int totalOperators = 0; //running total of operators
	protected int totalOperands = 0; //running total of operands
	
	//This is a flag to turn the aggregation feature on and off. In aggregation mode, internal method declarations are considered as one with their calling method.
	// For example, in aggregation mode, internal methods are not given their own data point, and their complexity is added to the complexity of the caller.
	//TODO: Need to make a join method for aggregating method complexities. 
	private boolean aggregate = false; 
	private boolean printHeader = false;
	
	private boolean debug = false;
	
	public MethodWalker(String nameOfClass, boolean aggregateInternalMethods, boolean doPrintHeader) {
		aggregate = aggregateInternalMethods;
		className = nameOfClass;
		printHeader = doPrintHeader;
	}
	
	private void joinPaths(MethodWalker child) {
		loc += child.loc;
		linesOfComments += child.linesOfComments;
		
		for(String op : child.uniqueOperands) {
			uniqueOperands.add(op);
		}
		for(String op : child.uniqueOperators) {
			uniqueOperators.add(op);
		}
		totalOperands += child.totalOperands;
		totalOperators += child.totalOperators;
		
		mccabesComplexity += child.mccabesComplexity;
	}
	
	//TODO: add a bool flag to this call so that we know if we need to print a data point or not.
	public void walkMethod(MethodDeclaration methDef, boolean printDataPoint) {
		//TODO: method-level metrics go here
		//get the statements in the method body
		name = className + "." + methDef.getName();
		List<Statement> methodStatements = new ArrayList<Statement>();
		BlockStmt body = methDef.getBody();
		if(body != null) {
			methodStatements = body.getStmts();
		}
		//get the LOC for the method
		Range range = methDef.getRange();
		loc = range.end.line - range.begin.line - 1;//subtract 1 to get rid of the closing brace 
		
		for(Statement st : methodStatements) {
			walkStatement(st);
		}
		Log("\t" + name + formatParameters(methDef.getParameters()));
		Log("loc: " + loc);
		Log("comments: " + linesOfComments);
		rloc = loc - linesOfComments;
		Log("Runnable lines of code: " + rloc);
		Log("McCabe's Cyclomatic Complexity: " + mccabesComplexity);
		
		//printHalsteadStats();
		if(printDataPoint) {
			printDataPoint(printHeader);
		}
		
	}
	
	private void walkStatement(Statement st) {
		//if the statement is null, then we have come from a leaf node on the AST. Stop processing.
		if(st == null) {
			return;
		}
		//get the comments associated with this statement
		Comment comment = st.getComment();
		Range commentRange = null;
		if(comment != null) {
			commentRange = comment.getRange();
		}
		if(commentRange != null) {
			linesOfComments += (commentRange.end.line - commentRange.begin.line) + 1;
		}
		
		if(st instanceof ExpressionStmt) {
			Log("\t\t\tExpressionStmt: " + st);
			//This is just a generic expression, usually a composite of expressions really.
			ExpressionStmt exprStmt = (ExpressionStmt) st;
			walkExpression(exprStmt.getExpression());
		} else if(st instanceof AssertStmt) {
			//No clue what this is.
			Log("\t\t\tAssertStmt: " + st);
		} else if(st instanceof IfStmt) {
			//If conditional, pretty self-explanatory:
			// if(Condition) {Then}else
			//Note that a nested else-if will be an IfStmt returned in the Then of the parent if.
			Log("\t\t\tIfStmt: " + st);
			IfStmt ist = (IfStmt)st;
		
			//if statements contribute to CC
			mccabesComplexity++;
			walkExpression(ist.getCondition());
			walkStatement(ist.getThenStmt());
			walkStatement(ist.getElseStmt());
		} else if(st instanceof SwitchStmt) {
			//Switch case
			//switch(){case x: {} break;... }
			Log("\t\t\tSwitchStmt: " + st);
			SwitchStmt switchStmt = (SwitchStmt)st;
			
			//switch statements contribute to CC
			mccabesComplexity++;
			walkExpression(switchStmt.getSelector());
			List<SwitchEntryStmt> statements = switchStmt.getEntries();
			numberOfSwitchCases = 0; //begin with 0 cases
			if(statements != null) {
				for(Statement s : statements) {
					walkStatement(s);
				}
			}
		} else if(st instanceof SwitchEntryStmt) {
			//a case block within a switch
			//case Label: 
			Log("\t\t\tSwitchEntryStmt: " + st);
			SwitchEntryStmt switchEntry = (SwitchEntryStmt)st;
			
			//We are counting each statement beyond 1 as additional complexity. This may or may not be traditional.
			numberOfSwitchCases++;
			if(numberOfSwitchCases > 1) {
				mccabesComplexity++;
			}
			
			walkExpression(switchEntry.getLabel());
			List<Statement> entryStatements = switchEntry.getStmts();
			if(entryStatements != null) {
				for(Statement s : entryStatements) {
					walkStatement(s);
				}
			}
		} else if (st instanceof WhileStmt) {
			//While loop:
			//while(Condition){Body}
			Log("\t\t\tWhileStmt: " + st);
			WhileStmt whileStmt = (WhileStmt)st;
			
			//While statements contribute to complexity
			mccabesComplexity++;
			walkExpression(whileStmt.getCondition());
			walkStatement(whileStmt.getBody());
		} else if(st instanceof DoStmt) { 
			//Do loop
			//Do{Body}While{Condition}
			Log("\t\t\tDoStmt: " + st);
			DoStmt doStmt = (DoStmt)st;
			//Do statements contribute to complexity
			walkStatement(doStmt.getBody());
			walkExpression(doStmt.getCondition());
		} else if(st instanceof ForeachStmt) {
			//Foreach loop - for with an implicit enumerable iterator
			//for(Variable : Iterable) {Body}
			Log("\t\t\tForeachStmt: " + st);
			ForeachStmt foreachStmt = (ForeachStmt)st;
			
			//ForEach statements contribute to complexity
			mccabesComplexity++;
			walkExpression(foreachStmt.getIterable());
			walkExpression(foreachStmt.getVariable());
			walkStatement(foreachStmt.getBody());
		} else if(st instanceof ForStmt) {
			//For loop support
			//for(Init; Comparison; Update){Body}
			Log("\t\t\tForStmt: " + st);
			ForStmt forStmt = (ForStmt)st;
			
			//For statements contribute to complexity
			mccabesComplexity++;
			List<Expression> initialExprs = forStmt.getInit();
			if(initialExprs != null) {
				for(Expression e : initialExprs) {
					walkExpression(e);
				}
			}
			walkExpression(forStmt.getCompare());
			List<Expression> updateExprs = forStmt.getUpdate();
			if(updateExprs != null) {
				for(Expression e : updateExprs) {
					walkExpression(e);
				}
			}
			walkStatement(forStmt.getBody());
		} else if(st instanceof BlockStmt) {
			//Coupled series of statements
			//Statement;Statement;Statement;...
			Log("\t\t\tBlockStmt: " + st);
			BlockStmt bst = (BlockStmt)st;
			for(Statement s : bst.getStmts()) {
				walkStatement(s);
			}
		} else if(st instanceof TryStmt) {
			//Try block
			//try{TryBlock}Catchs{}FinallyBlock{}
			Log("\t\t\tTryStmt: " + st);
			TryStmt tryStmt = (TryStmt)st;
			
			walkStatement(tryStmt.getTryBlock());
			
			List<CatchClause> catchStmts = tryStmt.getCatchs();
			if(catchStmts != null) {
				for(CatchClause cat : catchStmts) {
					//each catch statement contributes to complexity
					mccabesComplexity++;
					walkStatement(cat.getCatchBlock());
				}
			}
			
			BlockStmt finallyBlock = tryStmt.getFinallyBlock();
			if(finallyBlock != null) {
				//finally blocks contribute to complexity
				mccabesComplexity++;
				walkStatement(finallyBlock);
			}
			
		} else if(st instanceof SynchronizedStmt) {
			Log("\t\t\tSynchronizedStmt: " + st);
			SynchronizedStmt syncStmt = (SynchronizedStmt)st;
			
			walkExpression(syncStmt.getExpr());
			walkStatement(syncStmt.getBlock());
		} else if(st instanceof ExplicitConstructorInvocationStmt) {
			Log("\t\t\tExplicitConstructorInvocationStmt: " + st);
			//TODO: I have no idea what this is. Just parsing it for now. need to investigate.
			ExplicitConstructorInvocationStmt conStmt = (ExplicitConstructorInvocationStmt)st;
			List<Expression> args = conStmt.getArgs();
			if(args != null) {
				for(Expression e : args) {
					walkExpression(e);
				}
			}
			
			walkExpression(conStmt.getExpr());
		} else if(st instanceof BreakStmt) {
			Log("\t\t\tBreakStmt: " + st);
			//TODO: This is just a break. It is definitely needed in CC, but Halstead? Meh? 
			BreakStmt brStmt = (BreakStmt)st;
		} else if(st instanceof ContinueStmt) {
			Log("\t\t\tContinueStmt: " + st);
			//TODO: This is just a continue. It is definitely needed in CC, but Halstead? dunno.
			ContinueStmt cntStmt = (ContinueStmt)st;
		} else if(st instanceof EmptyStmt) {
			Log("\t\t\tEmptyStmt: " + st);
			//TODO: I feel pretty strongly about not doing anything at all with this. Not entirely sure the point. Need to think about HM and CC more in light of this...
		} else if(st instanceof ReturnStmt) {
			Log("\t\t\tReturnStmt: " + st);
			ReturnStmt retStmt = (ReturnStmt)st;
			numberOfReturns++; // this is a new return statement
			if(numberOfReturns > 1) {
				// if this is an "additional" return statement, it contributes to the complexity. A final return does NOT.
				mccabesComplexity++; 
			}

			walkExpression(retStmt.getExpr());
		} else if(st instanceof LabeledStmt) {
			Log("\t\t\tLabeledStmt: " + st);
			LabeledStmt labStmt = (LabeledStmt)st;
			//TODO: labels. How do we factor that into CC. Is anyone crazy enough to use them? Is that an OP? In standard assembly a label is a NOOP, but the adjoining jump is an OP, does that translate?
			walkStatement(labStmt.getStmt());
		} else if(st instanceof ThrowStmt) {
			Log("\t\t\tThrowStmt: " + st);
			//Throw statements contribute to complexity
			mccabesComplexity++;
			ThrowStmt throwStmt = (ThrowStmt)st;
			walkExpression(throwStmt.getExpr());
		}
	}
	
	private void walkExpression(Expression ex) {
		if(ex instanceof ConditionalExpr) {
			Log("Conditional Expression: " + ex);
			ConditionalExpr condExpr = (ConditionalExpr)ex;
			walkExpression(condExpr.getCondition());
			walkExpression(condExpr.getThenExpr());
			walkExpression(condExpr.getElseExpr());
			
		} else if (ex instanceof BinaryExpr) {
			BinaryExpr bin = (BinaryExpr)ex;
			Log("Binary Expression: " + ex);
			Log("BINARY: " + bin);
			Log("OP: " + bin.getOperator());
			uniqueOperators.add(bin.getOperator().toString());
			//each non-unique operator contributes to complexity
			mccabesComplexity++;
			uniqueOperands.add(bin.getLeft().toString());
			uniqueOperands.add(bin.getRight().toString());
			
			//update Halstead totals: one operator, two operands
			totalOperators++;
			totalOperands += 2;
			
			walkExpression(bin.getLeft());
			walkExpression(bin.getRight());
		} else if (ex instanceof NameExpr) {
			Log("Name Expression: " + ex);
			NameExpr nameExpr = (NameExpr)ex;
		} else if (ex instanceof VariableDeclarationExpr) {
			Log("Variable Declaration Expression: " + ex);
			VariableDeclarationExpr dec = (VariableDeclarationExpr)ex;
			Log("TYPE: " + dec.getType());
			
			List<VariableDeclarator> vars = dec.getVars();
			if(vars != null) {
				for(VariableDeclarator vd : vars) {
					walkExpression(vd.getInit());
				}
			}
		} else if(ex instanceof AssignExpr) {
			AssignExpr assign = (AssignExpr)ex;
			Log("Assign Expression: " + ex);
			Log("OP: " + assign.getOperator());
			//each non-unique operator contributes to complexity
			mccabesComplexity++;
			walkExpression(assign.getTarget());
			walkExpression(assign.getValue());
		} else if (ex instanceof AnnotationExpr) {
			Log("Annotation Expression: " + ex);
			AnnotationExpr annotationExpr = (AnnotationExpr)ex;
			//TODO: This doesn't matter for V1, but may need it in the future
		} else if (ex instanceof ArrayAccessExpr) {
			Log("Array Access Expression: " + ex);
			ArrayAccessExpr arrayAccessExpr = (ArrayAccessExpr)ex;
			walkExpression(arrayAccessExpr.getIndex());
			walkExpression(arrayAccessExpr.getName());
		} else if (ex instanceof ArrayCreationExpr) {
			Log("Array Creation Expression: " + ex);
			ArrayCreationExpr arrayCreationExpr = (ArrayCreationExpr)ex;
			walkExpression(arrayCreationExpr.getInitializer());
			List<Expression> dims = arrayCreationExpr.getDimensions();
			for(Expression dim : dims) {
				walkExpression(dim);
			}
		} else if (ex instanceof ArrayInitializerExpr) {
			Log("Array Initializer Expression: " + ex);
			ArrayInitializerExpr arrayInitializerExpr = (ArrayInitializerExpr)ex;
			List<Expression> values = arrayInitializerExpr.getValues();
			for(Expression val : values) {
				walkExpression(val);
			}
		} else if (ex instanceof CastExpr) {
			Log("Cast Expression: " + ex);
			CastExpr castExpr = (CastExpr)ex;
			walkExpression(castExpr.getExpr());
		} else if (ex instanceof ClassExpr) {
			Log("Class Expression: " + ex);
			ClassExpr classExpr = (ClassExpr)ex;
			//TODO: This is another expression that doesn't really help with DP calculations for V1. still could be useful in the future.
		} else if (ex instanceof EnclosedExpr) {
			Log("Enclosed Expression: " + ex);
			EnclosedExpr enclosedExpr = (EnclosedExpr)ex;
			walkExpression(enclosedExpr.getInner());
		} else if (ex instanceof FieldAccessExpr) {
			Log("Field Accessor Expression: " + ex);
			FieldAccessExpr fieldAccessExpr = (FieldAccessExpr)ex;
			walkExpression(fieldAccessExpr.getScope());
			Log("FIELD: " + fieldAccessExpr.getField());
		} else if (ex instanceof InstanceOfExpr) {
			Log("InstanceOf Expression: " + ex);
			InstanceOfExpr instanceOfExpr = (InstanceOfExpr)ex;
			walkExpression(instanceOfExpr.getExpr());
			//TODO: Do we count the type in an instanceof statement as a contributing operand? I kind of think so. (x instanceof y) is roughly equivalent to (x = y) in terms of complexity....
		} else if (ex instanceof LambdaExpr) {
			Log("Lambda Expression: " + ex);
			LambdaExpr lambdaExpr = (LambdaExpr)ex;
			walkStatement(lambdaExpr.getBody());
		} else if (ex instanceof LiteralExpr) {
			Log("Literal Expression: " + ex);
			LiteralExpr literalExpr = (LiteralExpr)ex;
		} else if (ex instanceof MethodCallExpr) {
			Log("Method Call Expression: " + ex);
			MethodCallExpr methodCallExpr = (MethodCallExpr)ex;
			walkExpression(methodCallExpr.getNameExpr());
			List<Expression> arguments = methodCallExpr.getArgs();
			if(arguments != null) {
				for(Expression arg : arguments) {
					walkExpression(arg);
				}
			}
			
			walkExpression(methodCallExpr.getScope());
		} else if(ex instanceof MethodReferenceExpr) {
			Log("Method Reference Expression: " + ex);
			MethodReferenceExpr methodReferenceExpr = (MethodReferenceExpr)ex;
			walkExpression(methodReferenceExpr.getScope());
			Log("IDENTIFIER: " + methodReferenceExpr.getIdentifier());
		} else if(ex instanceof ThisExpr) {
			Log("This Expression: " + ex);
			ThisExpr thisExpr = (ThisExpr)ex;
			walkExpression(thisExpr.getClassExpr()); //TODO: not sure what this is. Need to check
		} else if (ex instanceof TypeExpr) {
			Log("Type Expression: " + ex);
			TypeExpr typeExpr = (TypeExpr)ex;
			Log("TYPE: " + typeExpr.getType());
		} else if(ex instanceof ObjectCreationExpr) {
			Log("Object Creation Expression: " + ex);
			ObjectCreationExpr objectCreationExpr = (ObjectCreationExpr)ex;
			List<Expression> args = objectCreationExpr.getArgs();
			if(args != null) {
				for(Expression arg : args) {
					walkExpression(arg);
				}
			}
			List<BodyDeclaration> bodyDeclarations = objectCreationExpr.getAnonymousClassBody();
			//TODO: this could be made more generic: a separate, recursive class walking class? Needs to be statefull... not static.
			if(bodyDeclarations != null) {
				for(BodyDeclaration bd : bodyDeclarations) {
					if(bd instanceof MethodDeclaration) {
						MethodWalker anonymousMethodWalker = new MethodWalker(objectCreationExpr.getType().toString(), aggregate, printHeader);
						//Only print the datapoint if we are not aggregating to the caller.
						anonymousMethodWalker.walkMethod((MethodDeclaration)bd, !aggregate);
						
						if(aggregate) {
							joinPaths(anonymousMethodWalker);
						}else {
							//if we aren't aggregating, then we just printed the header. No need for another one.
							printHeader = false;
						}
					}
				}
			}
			walkExpression(objectCreationExpr.getScope());
		} else if(ex instanceof SuperExpr) {
			Log("Super Expression: " + ex);
			SuperExpr superExpr = (SuperExpr)ex;
			walkExpression(superExpr.getClassExpr());
		} else if(ex instanceof UnaryExpr) {
			Log("Unary Expression: " + ex);
			UnaryExpr unaryExpr = (UnaryExpr)ex;
			uniqueOperands.add(unaryExpr.getExpr().toString());
			walkExpression(unaryExpr.getExpr());
			uniqueOperators.add(unaryExpr.getOperator().toString());
			//update Halstead totals: one operator, one operand
			totalOperators++;
			totalOperands++;
			Log(unaryExpr.getOperator().toString());
			//each non-unique operator contributes to complexity
			mccabesComplexity++;
		}
		
	}
	
	private int getNumberOfUniqueOperators() {
		return uniqueOperators.size();
	}
	
	private int getNumberOfUniqueOperands() {
		return uniqueOperands.size();
	}
	
	private int getProgramVocabulary() {
		return getNumberOfUniqueOperators() + getNumberOfUniqueOperands();
	}
	
	private int getProgramLength() {
		return totalOperators + totalOperands;
	}
	
	private double getCalculatedProgramLength() {
		int n1 = getNumberOfUniqueOperators();
		int n2 = getNumberOfUniqueOperands();
		return n1*(Math.log(n1)/Math.log(2)) + n2*(Math.log(n2)/Math.log(2)); 
	}
	
	private double getProgramVolume() {
		return getProgramLength() + (Math.log(getProgramVocabulary())/Math.log(2)); 
	}
	
	private double getProgramDifficulty() {
		if(getNumberOfUniqueOperands() == 0) {
			return Double.NaN;
		}
		return (getNumberOfUniqueOperators()/2) * (totalOperands/getNumberOfUniqueOperands());
	}
	
	private double getProgramEffort() {
		return getProgramDifficulty() * getProgramVolume();
	}
	
	private double getHalsteadTimeToCode() {
		return getProgramEffort() / 18; //units are in seconds
	}
	
	private double getHalsteadDeliveredBugs() {
		return Math.pow(getProgramEffort(), (double)2/3)/3000;
	}
	
	private void printHalsteadStats() {
		System.out.println("Halstead Metrics: ");
		System.out.println("Distinct Operators: " + getNumberOfUniqueOperators());
		System.out.println("Distinct Operands: " + getNumberOfUniqueOperands());
		System.out.println("Total Operators: " + totalOperators);
		System.out.println("Total Operands: " + totalOperands);
		System.out.println("Program Vocabulary: " + getProgramVocabulary());
		System.out.println("Program Length: " + getProgramLength());
		System.out.println("Calculated Program Length (normalized log-transform): " + getCalculatedProgramLength());
		System.out.println("Volume: " + getProgramVolume());
		System.out.println("Difficulty: " + getProgramDifficulty());
		System.out.println("Effort: " + getProgramEffort());
		System.out.println("Halstead's Time to Code: " + getHalsteadTimeToCode());
		System.out.println("Halstead's Shipped Bugs: " + getHalsteadDeliveredBugs());
	}
	
	private void printDataPoint(boolean withHeader) {
		if(withHeader) {
			System.out.println("Name, LOC, RLOC, McCabeCC, n1, n2, N1, N2, Vocabulary, Length, CLength, Volume, Difficulty, Effort, TimeToCode, ShippedBugs");
		}
		
		System.out.println(String.format("%s, %d, %d, %d %d, %d, %d, %d, %d, %d, %f, %f, %f, %f, %f, %f", 
										name, loc, rloc, mccabesComplexity, getNumberOfUniqueOperators(), getNumberOfUniqueOperands(),
										totalOperators, totalOperands, getProgramVocabulary(), getProgramLength(),
										getCalculatedProgramLength(), getProgramVolume(), getProgramDifficulty(),
										getProgramEffort(), getHalsteadTimeToCode(), getHalsteadDeliveredBugs()));
	}
	
	private void Log(String s) {
		if(debug) {
			System.out.println(s);
		}
	}
	private String formatParameters(List<Parameter> parameters) {
		String returnStr = "";
		if(parameters != null) {
			for(Parameter p : parameters) {
				String paramType = p.toString().split(" ")[0];
				returnStr += paramType + ", ";
			}
			if(returnStr.length() > 2) {
				returnStr = returnStr.substring(0, returnStr.length()-2);
			}
		}
		return String.format("(%s)", returnStr);
	}
	
	class DataBlob {
		public int linesOfComments;
		public int loc;
		public int totalOperators;
		public int uniqueOperators;
		public int totalOperands;
		public int uniqueOperands;
	}
}
