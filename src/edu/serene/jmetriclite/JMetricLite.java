package edu.serene.jmetriclite;

import java.io.File;
import java.io.IOException;
import edu.serene.jmetriclite.parsing.ClassDecomposer;
import com.github.javaparser.ParseException;

public class JMetricLite {

	//TODO: take in a project directory and:
	//	- find the Java files
	//	- figure out if they are duplicates
	//  - form a hash list of classes, associated methods, and implementations
	
	//toggle this for aggregating fully contained class implementations within a calling method
	private static boolean aggregate = true;
	public static void main(String[] args) {
		if(args.length < 2) {
				System.err.println("Error: please supply the root project directory to analyze");
				System.exit(1);
		}
		aggregate = Boolean.parseBoolean(args[1]);
		processRawFiles(args[0]);
	}
	
	public static void processRawFiles(final String path) {
		File folder = new File(path);
		//TODO: there is a bug here. Program crashes if there is a flat root structure... not ideal, but no time to fix right now.
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	processRawFiles(fileEntry.getAbsolutePath());
	        } else {
	            if(fileEntry.getName().endsWith(".java")) {
	            	try {
						processJavaFile(fileEntry);
					} catch (ParseException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        }
	    }
	}
	
	public static void processJavaFile(File javaFile) throws ParseException, IOException {
		new ClassDecomposer(aggregate).listClasses(javaFile);
	}
}
